%% Initialize parameters
% Parameters from Aachen
par.m   = 0.933;        % body part mass [kg] 
par.R   = 0.04;         % radius of wheel [m]
par.L   = 0.085749;     % position of COM [m]
par.I   = 0.00686014;   % inertia of body part [kg*m^2]       
par.Bm  = 0;            % bearing damping ratio [N*m/(rad/s)]
par.g   = 9.81;         % gravity constant [m/s^2]
par.Tau = 0.0994;       % speed loop time constant (P=0.194775 I=3.387779)
par.K   = 1;%1.0071;       % speed loop gain
par.Ts  = 0.01;         % controller sampling time

% Define simulation parameters
par.t_sim = 3;
par.simopt.Solver = 'ode4';
par.simopt.SrcWorkspace = 'current';
EnablePlot = 1;
par.K_lqr = zeros(1,4);

% Initial conditions
phiw0   = 0;
dphiw0  = 0;
theta0  = 45 / 360 * 2 * pi;
dtheta0 = 0;

% Run simulation
par.modelname   = 'SegWayModel';
data            = fun_SegwayFigure(phiw0,dphiw0,theta0,dtheta0,par,EnablePlot);

%% Linearize system and design LQR controller
% Linearize
getLinearSegway

% Design LQR controller
Q           = diag([1 1e-2 1 1e-2]); % weighting matrix Q - penalize states
% Q = C'*C;
R           = 1e-2;                  % weighting matrix R 
par.K_lqr   = lqr(A,B,Q,R);          % LQR gain matrix
par.Kd_lqr  = lqrd(A,B,Q,R,par.Ts);  % time-discrete LQR gain matrix

% Initial conditions
phiw0   = -1;
dphiw0  = 0;
theta0  = -15/180*pi;
dtheta0 = 0;

% Run simulation
par.modelname   = 'SegWayModel';
data            = fun_SegwayFigure(phiw0,dphiw0,theta0,dtheta0,par,EnablePlot);


%% Design a lower-dimensional LQR controller that keeps the segway vertical

% Get linearization oc 3x3 system (first state "phiw" is not releant)
getLinearSegway_SISO

% Make LQR to simply keep segway vertical
Q = diag([0 1 0]);        % weighting matrix Q - penalize states [dphiw, theta, dtheta]
R = 1e2;                    % weighting matrix R 
% weird: R does not seem to have too much of an influence, only when very very small
K_lqr = lqr(A,B,Q,R);     % LQR gain matrix 
par.K_lqr = [0 K_lqr];
par.Kd_lqr  = [0 lqrd(A,B,Q,R,par.Ts)];  % time-discrete LQR gain matrix

% Initial conditions
phiw0   = -1;
dphiw0  = 0;
theta0  = -15/180*pi;
dtheta0 = 0;

% Run simulation
par.modelname   = 'SegWayModel';
data            = fun_SegwayFigure(phiw0,dphiw0,theta0,dtheta0,par,EnablePlot);

%% Linearize stabilized system and design a stabilizing PID controller
getLinearSegway_SISO_stabilized

fig =figure;
hlocus = rlocusplot(sys);
xlim([-20,20]);
ylim([-7 7]);

% design PID controller
% Use pidtool
% PD controller
% C =  Kp * (1 + Td * s)
%  with Kp = 0.86, Td = 0.261
par.Kp = 0.8596;
par.Td = 0.2607;
par.Ti = inf;

% PID controller
%                  1      1          
%  C =  Kp * (1 + ---- * --- + Td * s)
%                  Ti     s          
%   with Kp = 0.582, Ti = 5.1, Td = 0.213
%             par.Kp = 0.5820;
%             par.Td = 0.2132;
%             par.Ti = 5.1041;

% P-controller by hand
par.Kp = 0.5820;
par.Td = 0;
par.Ti = inf;

% Stability analysis
% pole-zero map of T(s) and Nyquist of L(s)
s = tf('s');
C = par.Kp*(1 + 1/par.Ti * 1/s + par.Td * s);
P = sys;
L = C*P;
T = minreal((L)/(1+L));

fig2 = figure;
% subplot(211);
% pzplot(T)
% subplot(212);
nyquist(L)

% Run simulation    
% Reference in x
par.xref = 0;

% Initial conditions
phiw0   = -1;
dphiw0  = 0;
theta0  = -15/180*pi;
dtheta0 = 0;

% Simulate
par.modelname = 'SegWayModelStabilized';
data = fun_SegwayFigure(phiw0,dphiw0,theta0,dtheta0,par,EnablePlot);

        
