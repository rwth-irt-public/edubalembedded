%% 3-Axis accelerometer calibration using least squares method.

nmeas = 10000; % Number of measurements for each axis configuration
W1=zeros(nmeas,4); % Initialize measurement vector 1
GX1 = 0; GY1 = 0; GZ1 = 0; % Initialize gyro acculmulator 1
W2=zeros(nmeas,4); % Initialize measurement vector 2
GX2 = 0; GY2 = 0; GZ2 = 0; % Initialize gyro acculmulator 2
W3=zeros(nmeas,4); % Initialize measurement vector 3
GX3 = 0; GY3 = 0; GZ3 = 0; % Initialize gyro acculmulator 3
W4=zeros(nmeas,4); % Initialize measurement vector 4
GX4 = 0; GY4 = 0; GZ4 = 0; % Initialize gyro acculmulator 4
W5=zeros(nmeas,4); % Initialize measurement vector 5
GX5 = 0; GY5 = 0; GZ5 = 0; % Initialize gyro acculmulator 5
W6=zeros(nmeas,4); % Initialize measurement vector 6
GX6 = 0; GY6 = 0; GZ6 = 0; % Initialize gyro acculmulator 6

%% Fill vectors for the given orientations:
% Z-Axis down
Y1 = repmat([0 0 1],nmeas,1); % Output vector for Zdown
load Zdown;
W1 = [ax ay az ones(size(ax))];
GX1 = mean(gx);
GY1 = mean(gy);
GZ1 = mean(gz);
% Z-Axis up
Y2 = repmat([0 0 -1],nmeas,1); % Output vector for Zup
load Zup;
W2 = [ax ay az ones(size(ax))];
GX2 = mean(gx);
GY2 = mean(gy);
GZ2 = mean(gz);
% Y-Axis down
Y3 = repmat([0 1 0],nmeas,1); % Output vector for Ydown
load Ydown;
W3 = [ax ay az ones(size(ax))];
GX3 = mean(gx);
GY3 = mean(gy);
GZ3 = mean(gz);
% Y-Axis up
Y4 = repmat([0 -1 0],nmeas,1); % Output vector for Yup
load Yup;
W4 = [ax ay az ones(size(ax))];
GX4 = mean(gx);
GY4 = mean(gy);
GZ4 = mean(gz);
% X-Axis down
Y5 = repmat([1 0 0],nmeas,1); % Output vector for Xdown
load Xdown;
W5 = [ax ay az ones(size(ax))];
GX5 = mean(gx);
GY5 = mean(gy);
GZ5 = mean(gz);
% X-Axis up
Y6 = repmat([-1 0 0],nmeas,1); % Output vector for Xup
load Xup;
W6 = [ax ay az ones(size(ax))];
GX6 = mean(gx);
GY6 = mean(gy);
GZ6 = mean(gz);
% Generate measurement and output matrices
Y = [Y1; Y2; Y3; Y4; Y5; Y6];
W = [W1; W2; W3; W4; W5; W6];
% Calculate the least square solution
X = (W.'*W)\(W.'*Y); % Calibration matrix
% Calculate mean value of gyro readings
Gxc = (GX1 + GX2 + GX3 + GX4 + GX5 + GX6)/6;
Gyc = (GY1 + GY2 + GY3 + GY4 + GY5 + GY6)/6;
Gzc = (GZ1 + GZ2 + GZ3 + GZ4 + GZ5 + GZ6)/6;