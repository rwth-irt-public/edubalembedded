function out = bin2single(in)
% %% Check for correct input length
if(length(in) ~= 32)
    error('Data input length must be 32.')
end
%% Typecast to double
in = double(in);
%% Calculate sign, exponent and mantissa (IEEE 754 standard)
frc = 1+sum(in(10:32).*2.^(-1:-1:-23));
pow = sum(in(2:9).*2.^(7:-1:0))-127;
sgn = (-1)^in(1);
%% Calculate output
out = sgn * frc * 2^pow;

% in = double(in); % typecast to double
% b = 127;         % exponent bias
% %% Calculate exponent IEEE 754 standard
% e = 0;           % initialize exponent
% for ii = 2:9
%     e = e + in(ii)*2^(9-ii)
% end
% e = e - b; % biased exponent 
% %% Calculate mantissa
% m = 0; % initialize mantissa 
% for ii=10:32
%     m = m + in(ii)*2^(-(ii-9));
% end
% m = m + 1; % standard calculation
% %% Calculate output
% out = (-1)^in(1)*(2^e)*m;

end
