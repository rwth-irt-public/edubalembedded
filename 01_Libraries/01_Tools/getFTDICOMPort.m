function [com_port_string, com_port_index] = getFTDICOMPort
%GETEXTERNALMODECOMPORT Finds the appropriate TI VCP COM port for external
%mode with Launchpad and returns it. This COM port changes with every
%Launchpad board on every connected computer.

% if contains(system_dependent('getos'), 'Windows 7')
    [~, com_device_string] = system('chgport');
    com_device_string_list = splitlines(com_device_string);
%     number_of_com_devices = length(com_device_string_list)-1; % subtract the empty string line
%     disp(['Found ' num2str(number_of_com_devices) ' COM ports']);
    com_port_index = find(contains(com_device_string_list, 'VCP0')); % TODO: Is this string the same on every computer?
    if isempty(com_port_index)
        error('Did not find external mode COM port. Is device connected and turned on?');
    end
    [start_i, end_i] = regexp(com_device_string_list{com_port_index}, 'COM[0-9]*');
    com_port_string = com_device_string_list{com_port_index}(start_i:end_i);
    disp(['Using ' com_port_string ' for external mode']);

%     
% else
%     error('Automatic identification of External mode COM port is only supported on > Windows 8');
% end

end

