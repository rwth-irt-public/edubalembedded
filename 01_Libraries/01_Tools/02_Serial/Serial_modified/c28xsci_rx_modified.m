function c28xsci_rx_modified(block)
%MSFUNTMPL_BASIC A Template for a Level-2 MATLAB S-Function
%   The MATLAB S-function is written as a MATLAB function with the
%   same name as the S-function. Replace 'msfuntmpl_basic' with the 
%   name of your S-function.
%
%   It should be noted that the MATLAB S-function is very similar
%   to Level-2 C-Mex S-functions. You should be able to get more
%   information for each of the block methods by referring to the
%   documentation for C-Mex S-functions.
%
%   Copyright 2003-2010 The MathWorks, Inc.

%%
%% The setup method is used to set up the basic attributes of the
%% S-function such as ports, parameters, etc. Do not add any other
%% calls to the main body of the function.
%%
setup(block);

%endfunction

%% Function: setup ===================================================
%% Abstract:
%%   Set up the basic characteristics of the S-function block such as:
%%   - Input ports
%%   - Output ports
%%   - Dialog parameters
%%   - Options
%%
%%   Required         : Yes
%%   C-Mex counterpart: mdlInitializeSizes
%%
function setup(block)
% Register number of ports
block.NumInputPorts  = 0;
block.NumOutputPorts = 2;

% Setup port properties to be inherited or dynamic
block.SetPreCompInpPortInfoToDynamic;
block.SetPreCompOutPortInfoToDynamic;

% Override output port properties
block.OutputPort(1).Dimensions  = 16;

block.OutputPort(1).DatatypeID  = 3; % uint8


block.OutputPort(1).Complexity  = 'Real';
block.OutputPort(1).SamplingMode='Sample';

% Override output port properties
block.OutputPort(2).Complexity  = 'Real';
block.OutputPort(2).SamplingMode='Sample';
block.OutputPort(2).Dimensions       = 1;
block.OutputPort(2).DatatypeID  = 5; % uint16


% Register parameters
block.NumDialogPrms     = 4;
%sciModule, sampleTime

% Register sample times
%  [0 offset]            : Continuous sample time
%  [positive_num offset] : Discrete sample time
%
%  [-1, 0]               : Inherited sample time
%  [-2, 0]               : Variable sample time
block.SampleTimes = [block.DialogPrm(2).Data 0];

% Specify the block simStateCompliance. The allowed values are:
%    'UnknownSimState', < The default setting; warn and assume DefaultSimState
%    'DefaultSimState', < Same sim state as a built-in block
%    'HasNoSimState',   < No sim state
%    'CustomSimState',  < Has GetSimState and SetSimState methods
%    'DisallowSimState' < Error out when saving or restoring the model sim state
  block.SimStateCompliance = 'DefaultSimState';
  block.RegBlockMethod('WriteRTW', @WriteRTW);
  block.RegBlockMethod('Outputs', @Outputs);    
  block.RegBlockMethod('PostPropagationSetup', @PostPropagationSetup);
  block.RegBlockMethod('InitializeConditions', @InitializeConditions);
function PostPropagationSetup(block)

%% Setup Dw
block.NumDworks                = 2;

block.Dwork(1).Name            = 'index';
block.Dwork(1).Dimensions      = 1;
block.Dwork(1).DatatypeID      = 0;
block.Dwork(1).Complexity      = 'Real';
block.Dwork(1).UsedAsDiscState = false;

block.Dwork(2).Name            = 'SignalLength';
block.Dwork(2).Dimensions      = 1;
block.Dwork(2).DatatypeID      = 0;
block.Dwork(2).Complexity      = 'Real';
block.Dwork(2).UsedAsDiscState = false;

function InitializeConditions(block)
block.Dwork(1).Data      = 1;
block.Dwork(2).Data      = length(block.DialogPrm(3).Data);

function Outputs(block)
output = zeros(1,block.OutputPort(1).Dimensions);
num = 0;


SimulationSignalLength = block.Dwork(2).Data;

nextidx = block.Dwork(1).Data;
if SimulationSignalLength >= nextidx
  nexttime = block.DialogPrm(3).Data(nextidx);
end

while (nextidx <= SimulationSignalLength)  && (nexttime <= block.currentTime) && (num < block.OutputPort(1).Dimensions)
   num = num+1;
   output(num) = block.DialogPrm(4).Data(nextidx);
   
   nextidx = nextidx+1;
   if SimulationSignalLength >= nextidx
    nexttime = block.DialogPrm(3).Data(nextidx);
   end
end

block.Dwork(1).Data = nextidx;



block.OutputPort(1).Data = cast(output,'uint8');
block.OutputPort(2).Data = cast(num,'uint16');

function WriteRTW(block)
% Save the parameters to an RTW file for code generation.
%p1 = sprintf('%f', block.DialogPrm(1).Data);


switch block.DialogPrm(1).Data
    case 1
        block.WriteRTWParam('string', 'sciModule', 'A');
    case 2
        block.WriteRTWParam('string', 'sciModule', 'B');
    case 3
        block.WriteRTWParam('string', 'sciModule', 'C');
end

%block.WriteRTWParam('matrix', 'dataType', block.DialogPrm(2).Data);

block.WriteRTWParam('matrix', 'sampleTime', block.DialogPrm(2).Data);
%block.WriteRTWParam('matrix', 'dataDim', block.DialogPrm(3).Data);%block.DialogPrm(3).Data

%sciModule, dataType,dataDim, sampleTime
%endfunction

%end Outputs

