
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def = legacy_code('initialize');
def.SourceFiles = {'Keyboard.c';};
def.HeaderFiles = {'Keyboard.h';};
def.SFunctionName = 'readKey_sf';


def.OutputFcnSpec ='int16 y1  = KeyboardInput(int32 p1)';
def.StartFcnSpec = '';
def.TerminateFcnSpec = '';


legacy_code('sfcn_cmex_generate',def);
legacy_code('compile',def);
legacy_code('slblock_generate',def,'GeneratedBlocks');
legacy_code('sfcn_tlc_generate',def);
legacy_code('rtwmakecfg_generate', def)