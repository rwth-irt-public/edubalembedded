//#include <stdio.h>
#include <windows.h>
//#include <initCOM.h>
//#include <tmwtypes.h>


// Need to tell Microsoft Compiler (e.g. Visual Studio to link user32.lib Library
// Not necessary e.g. for using Eclipse Toolchain
#ifdef _MSC_VER             
#pragma comment(lib, "user32")  
#endif

short KeyboardInput(int nVirtKey)
{
return GetKeyState(nVirtKey);
}
