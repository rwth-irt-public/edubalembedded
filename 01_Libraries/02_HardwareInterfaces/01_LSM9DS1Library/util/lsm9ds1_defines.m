% Based on Adafruit_LSM9DS1_Library - https://github.com/adafruit/Adafruit_LSM9DS1

LSM9DS1.ADDRESS_MAG                 = uint8(hex2dec('1E'));         % 3B >> 1 = 7bit default
LSM9DS1.ADDRESS_ACCELGYRO           = uint8(hex2dec('6B'));             % D6 >> 1 = 7bit default

LSM9DS1.XG_ID                       = uint8(104); % 0b01101000
LSM9DS1.MAG_ID                      = uint8(61); % 0b00111101

LSM9DS1.SENSORID_ACCEL              = uint8(1);
LSM9DS1.SENSORID_MAG                = uint8(2);
LSM9DS1.SENSORID_GYRO               = uint8(3);
LSM9DS1.SENSORID_TEMP               = uint8(4);

% Accelerometer/gyro registers
LSM9DS1.REGISTER_WHO_AM_I_XG         = uint8(hex2dec('0F'));
LSM9DS1.REGISTER_CTRL_REG1_G         = uint8(hex2dec('10'));
LSM9DS1.REGISTER_CTRL_REG2_G         = uint8(hex2dec('11'));
LSM9DS1.REGISTER_CTRL_REG3_G         = uint8(hex2dec('12'));
LSM9DS1.REGISTER_ORIENT_CFG_G        = uint8(hex2dec('13'));
LSM9DS1.REGISTER_TEMP_OUT_L          = uint8(hex2dec('15'));
LSM9DS1.REGISTER_TEMP_OUT_H          = uint8(hex2dec('16'));
LSM9DS1.REGISTER_STATUS_REG          = uint8(hex2dec('17'));
LSM9DS1.REGISTER_OUT_X_L_G           = uint8(hex2dec('18'));
LSM9DS1.REGISTER_OUT_X_H_G           = uint8(hex2dec('19'));
LSM9DS1.REGISTER_OUT_Y_L_G           = uint8(hex2dec('1A'));
LSM9DS1.REGISTER_OUT_Y_H_G           = uint8(hex2dec('1B'));
LSM9DS1.REGISTER_OUT_Z_L_G           = uint8(hex2dec('1C'));
LSM9DS1.REGISTER_OUT_Z_H_G           = uint8(hex2dec('1D'));
LSM9DS1.REGISTER_CTRL_REG4           = uint8(hex2dec('1E'));
LSM9DS1.REGISTER_CTRL_REG5_XL        = uint8(hex2dec('1F'));
LSM9DS1.REGISTER_CTRL_REG6_XL        = uint8(hex2dec('20'));
LSM9DS1.REGISTER_CTRL_REG7_XL        = uint8(hex2dec('21'));
LSM9DS1.REGISTER_CTRL_REG8           = uint8(hex2dec('22'));
LSM9DS1.REGISTER_CTRL_REG9           = uint8(hex2dec('23'));
LSM9DS1.REGISTER_CTRL_REG10          = uint8(hex2dec('24'));

LSM9DS1.REGISTER_OUT_X_L_XL          = uint8(hex2dec('28'));
LSM9DS1.REGISTER_OUT_X_H_XL          = uint8(hex2dec('29'));
LSM9DS1.REGISTER_OUT_Y_L_XL          = uint8(hex2dec('2A'));
LSM9DS1.REGISTER_OUT_Y_H_XL          = uint8(hex2dec('2B'));
LSM9DS1.REGISTER_OUT_Z_L_XL          = uint8(hex2dec('2C'));
LSM9DS1.REGISTER_OUT_Z_H_XL          = uint8(hex2dec('2D'));

% Magnetometer registers
LSM9DS1.REGISTER_WHO_AM_I_M         = uint8(hex2dec('0F'));
LSM9DS1.REGISTER_CTRL_REG1_M        = uint8(hex2dec('20'));
LSM9DS1.REGISTER_CTRL_REG2_M        = uint8(hex2dec('21'));
LSM9DS1.REGISTER_CTRL_REG3_M        = uint8(hex2dec('22'));
LSM9DS1.REGISTER_CTRL_REG4_M        = uint8(hex2dec('23'));
LSM9DS1.REGISTER_CTRL_REG5_M        = uint8(hex2dec('24'));
LSM9DS1.REGISTER_STATUS_REG_M       = uint8(hex2dec('27'));
LSM9DS1.REGISTER_OUT_X_L_M          = uint8(hex2dec('28'));
LSM9DS1.REGISTER_OUT_X_H_M          = uint8(hex2dec('29'));
LSM9DS1.REGISTER_OUT_Y_L_M          = uint8(hex2dec('2A'));
LSM9DS1.REGISTER_OUT_Y_H_M          = uint8(hex2dec('2B'));
LSM9DS1.REGISTER_OUT_Z_L_M          = uint8(hex2dec('2C'));
LSM9DS1.REGISTER_OUT_Z_H_M          = uint8(hex2dec('2D'));
LSM9DS1.REGISTER_CFG_M              = uint8(hex2dec('30'));
LSM9DS1.REGISTER_INT_SRC_M          = uint8(hex2dec('31'));

% Linear Acceleration: mg per LSB
LSM9DS1.ACCEL_MG_LSB_2G             = 0.061;
LSM9DS1.ACCEL_MG_LSB_4G             = 0.122;
LSM9DS1.ACCEL_MG_LSB_8G             = 0.244;
LSM9DS1.ACCEL_MG_LSB_16G            = 0.732;

% Accelerometer range
LSM9DS1.ACCELRANGE_2G               = uint8(bitshift(0,3));
LSM9DS1.ACCELRANGE_16G              = uint8(bitshift(1,3));
LSM9DS1.ACCELRANGE_4G               = uint8(bitshift(2,3));
LSM9DS1.ACCELRANGE_8G               = uint8(bitshift(3,3));

% Accelerometer data rate
LSM9DS1.ACCELDATARATE_POWERDOWN               = uint8(bitshift(0,4));
LSM9DS1.ACCELDATARATE_3_125HZ                 = uint8(bitshift(1,4));
LSM9DS1.ACCELDATARATE_6_25HZ                  = uint8(bitshift(2,4));
LSM9DS1.ACCELDATARATE_12_5HZ                  = uint8(bitshift(3,4));
LSM9DS1.ACCELDATARATE_25HZ                    = uint8(bitshift(4,4));
LSM9DS1.ACCELDATARATE_50HZ                    = uint8(bitshift(5,4));
LSM9DS1.ACCELDATARATE_100HZ                   = uint8(bitshift(6,4));
LSM9DS1.ACCELDATARATE_200HZ                   = uint8(bitshift(7,4));
LSM9DS1.ACCELDATARATE_400HZ                   = uint8(bitshift(8,4));
LSM9DS1.ACCELDATARATE_800HZ                   = uint8(bitshift(9,4));
LSM9DS1.ACCELDATARATE_1600HZ                  = uint8(bitshift(10,4));

% Magnetic Field Strength: gauss range
LSM9DS1.MAG_MGAUSS_4GAUSS           = 0.14;
LSM9DS1.MAG_MGAUSS_8GAUSS           = 0.29;
LSM9DS1.MAG_MGAUSS_12GAUSS          = 0.43;
LSM9DS1.MAG_MGAUSS_16GAUSS          = 0.58;

% Magnetometer gain
LSM9DS1.MAGGAIN_4GAUSS              = uint8(bitshift(0, 5));  % +/- 4 gauss
LSM9DS1.MAGGAIN_8GAUSS              = uint8(bitshift(1, 5));  % +/- 8 gauss
LSM9DS1.MAGGAIN_12GAUSS             = uint8(bitshift(2, 5));  % +/- 12 gauss
LSM9DS1.MAGGAIN_16GAUSS             = uint8(bitshift(3, 5));  % +/- 2 gauss

% Magnetometer datarate
LSM9DS1.MAGDATARATE_3_125HZ                  = uint8(bitshift(0,2));
LSM9DS1.MAGDATARATE_6_25HZ                   = uint8(bitshift(1,2));
LSM9DS1.MAGDATARATE_12_5HZ                   = uint8(bitshift(2,2));
LSM9DS1.MAGDATARATE_25HZ                     = uint8(bitshift(3,2));
LSM9DS1.MAGDATARATE_50HZ                     = uint8(bitshift(4,2));
LSM9DS1.MAGDATARATE_100HZ                    = uint8(bitshift(5,2));

% Angular Rate: dps per LSB
LSM9DS1.GYRO_DPS_DIGIT_245DPS       = 0.00875;
LSM9DS1.GYRO_DPS_DIGIT_500DPS       = 0.01750;
LSM9DS1.GYRO_DPS_DIGIT_2000DPS      = 0.07000;

% Gyro scale
LSM9DS1.GYROSCALE_245DPS            = uint8(bitshift(0, 3));  % +/- 245 degrees per second rotation
LSM9DS1.GYROSCALE_500DPS            = uint8(bitshift(1, 3));  % +/- 500 degrees per second rotation
LSM9DS1.GYROSCALE_2000DPS           = uint8(bitshift(3, 3));   % +/- 2000 degrees per second rotation

% Temperature: LSB per degree celsius
LSM9DS1.TEMP_LSB_DEGREE_CELSIUS     = 8;