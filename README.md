# EduBalEmbedded

This repository contains all necessary files to build **EduBal** (*Edu*cational *Bal*ancing Robot), an open-source balancing robot developed by the [Institute of Automatic Control](https://www.irt.rwth-aachen.de/) at RWTH Aachen University. The robot is designed to be low-cost, safe and easy to use for teaching control and system theory to students. Using Simulink, students can quickly implement their own control algorithms on the robot. Individual control parameters can be tuned online while analyzing the resulting behavior in live signal plots. At RWTH Aachen University and ETH Zurich 28 units have so far been built and used in control classes.

![alt text](img/edubal_photo.jpg "Front photo of EduBal robot")

This repository contains:
* Robot chassis hardware design files
* Circuit hardware design files
* Control software

## System requirements
The following software is necessary to develop with the robot:
* Matlab R2019a
* Embedded Coder Support Package for C2000
* Texas Instruments Code Composer v6
* Texas Instruments controlSUITE

The robot chassis and circuit were designed in:
* CATIA V5
* Autodesk EAGLE 9.4.0

## Reference
If you find this work useful, please cite the EduBal paper:
```
@misc{framing2020edubal,
    title={EduBal: An open balancing robot platform for teaching control and system theory},
    author={Christian-Eike Framing and Raffael Hedinger and Emmanuel Santiago Iglesias and Frank-Josef Heßeler and Dirk Abel},
    year={2020},
    eprint={2005.09304},
    archivePrefix={arXiv},
    primaryClass={eess.SY}
}
```