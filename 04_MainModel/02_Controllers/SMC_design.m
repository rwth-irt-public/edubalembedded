%% constant parameters
R = 0.04;           % radius of wheel [m]
L = 0.085749;       % position of COM [m]
Bm = 0;             % bearing damping ratio [N*m/(rad/s)]
g = 9.81;           % gravity constant [m/s^2]
Ts = 0.01;        
 
b_wheel = 0;
I_wheel = 0;
m_wheel = 0;
    
% K is uncertain but K has no influences on A
K = 1.0071;         % speed loop gain
                                  
% =========================================================================
% Parameters with uncertainty

% body part mass [kg] 
m_min = 0.5; 
m_max = 1.5;
m = ureal('m', 0.933, 'range', [m_min, m_max]);          
% m increase, f2, f3 increase
% A_max corresponds m_max

% -------------------------------------------------------------------------
% inertia of body part [kg*m^2]

% I decrease, f2, f3 increase
I_percentage = 20;
I = ureal('I', 0.00686014, 'percentage', I_percentage);            
I_min = I.range(1);
I_max = I.range(2);
% A_max corresponds I_min

% -------------------------------------------------------------------------
% speed loop time constant (P=0.194775 I=3.387779)

% Tau decrease, f2 increase
Tau_percentage = 20;
Tau = ureal('Tau', 0.0994, 'percentage', Tau_percentage);       
Tau_min = Tau.range(1);
Tau_max = Tau.range(2);
% A_max corresponds Tau_min


%% State-space representation of the system 

% State variable: [Phi, Phidot, Theta, Thetadot]'  
% System matrix
A = [ 0          1                                0                0           ;...
      0        -1/Tau                             0                0           ;...
      0          0                                0                1           ;...
      0     (Bm+m*R*L/Tau)/(I+m*L^2)         m*g*L/(I+m*L^2)   -Bm/(I+m*L^2)  ];
  
% Input matrix
B = [0;     K/Tau;      0;      -K*m*R*L/(Tau*(I+m*L^2))];

% Output matrix
C = [1 0 0 0; 0 0 1 0];

% Feedforward matrix
D = [0; 0];

% A(1,:)=[];
% A(:,1)=[];
% B(1,:)=[];
% C(:,1)=[];

sys = ss(A,B,C,D);

% -------------------------------------------------------------------------
% Nominal System

sys_nominal = sys.NominalValue;

% for sliding mode control the system should be transformed into controller 
% normal form and the states variable will lose its phisical meanings
sys_rnf = rn_form(sys_nominal, 'RNF');

% Normal form of the nominal system

A_ = sys_rnf.A;
B_ = sys_rnf.B;
C_ = sys_rnf.C;
D_ = sys_rnf.D;

% Coefficients of the transformation matrix of the nominal system

f1_ = A_(4,1);
f2_ = A_(4,2);
f3_ = A_(4,3);
f4_ = A_(4,4);

% -------------------------------------------------------------------------
% System in the minimal limit (A-, B-)

Amin = [ 0          1                                                       0                       0           ;...
         0        -1/Tau_max                                                0                       0           ;...
         0          0                                                       0                       1           ;...
         0     (Bm+m_min*R*L/Tau_max)/(I_max+m_min*L^2)         m_min*g*L/(I_max+m_min*L^2)   -Bm/(I_max+m_min*L^2)  ];

Bmin = [0;      K/Tau_max;      0;      -K*m_min*R*L/(Tau_max*(I_max+m_min*L^2))];
% Amin(1,:)=[];
% Amin(:,1)=[];
% Bmin(1,:)=[];

sysmin = ss(Amin,Bmin,C,D);

% Normal form of the system in the uncertain lower limit

sysmin_rnf = rn_form(sysmin, 'RNF');
A_min_rnf = sysmin_rnf.A;

% -------------------------------------------------------------------------
% System in the maximal limit (A+, B+)

Amax = [ 0          1                                                            0                0           ;...
         0        -1/Tau_min                                                     0                0           ;...
         0          0                                                            0                1           ;...
         0     (Bm+m_max*R*L/Tau_min)/(I_min+m_max*L^2)         m_max*g*L/(I_min+m_max*L^2)   -Bm/(I_min+m_max*L^2)  ];

Bmax = [0;K/Tau_min;0;-K*m_max*R*L/(Tau_min*(I_min+m_max*L^2))];
% Amax(1,:)=[];
% Amax(:,1)=[];
% Bmax(1,:)=[];

sysmax = ss(Amax,Bmax,C,D);

% Normal form of the system in the uncertain upper limit

sysmax_rnf = rn_form(sysmax, 'RNF');
A_max_rnf = sysmax_rnf.A;

% -------------------------------------------------------------------------
% Plot bode diagram of the system and uncertainties

figure(1)
bode(sys, sysmax, 'r', sysmin, 'g'); legend;

% -------------------------------------------------------------------------
% Define possible variation of the system (eq 4.31)

dA = A_max_rnf - A_;    % this matrix is tranformed using the RNF
%dA = A_ - A_min_rnf;    % this matrix is tranformed using the RNF

% Coefficients of the transformation matrix of the uncertain system

f1_var = dA(4,1);
f2_var = dA(4,2);
f3_var = dA(4,3);
f4_var = dA(4,4);

% -------------------------------------------------------------------------
% Define the uncertainty limits

phi_max = 10*pi/180;        % rad
phidot_max = 0.1;             % rad/s
theta_max = 15*pi/180;      % rad
thetadot_max = pi;          % rad/s

% Calculate transformation matrix

Tr = rn_form(sys_nominal, 'Tr');   

% Transformation of the uncertainty limits 

x_max = [phi_max, phidot_max, theta_max, thetadot_max]';
x_max_rnf = Tr * x_max;

%% sliding mode control

% Calculate the total maximal uncertainty

%alpha = abs(f1_var*x_max_rnf(1)) + abs(f2_var*x_max_rnf(2)) + abs(f3_var*x_max_rnf(3)) + abs(f4_var*x_max_rnf(4));
alpha = dA(4,1:4) * x_max_rnf;

% Define the safaty factor

eta = 9;   

% Calculate the SMC controller

k = alpha + eta;

schlauch = 4e-1; %Sättigungsbereich der Sättigungsfunktion sat


% sliding surface
% Es muss jedoch darauf geachtet werden, dass die gewünschte Dynamik durch 
% das System realisiert werden kann und die Parameter nicht zu groß gewählt 
% werden, um eine Anregung höherer Dynamiken zu vermeiden
%%%%%%%%%%%%%%%%%
% here the sliding surface is decided by pole placement
% p1 = -5;%-6;
% p2 = -0.02+0.82i;%-5; 
% p3 = -0.02-0.82i;%-4;  

p1 = -8;
p2 = -7; 
p3 = -6;  

coe = conv([1, -p1], conv([1, -p2],[1, -p3]));

lamda3 = coe(2);    % 
lamda2 = coe(3);
lamda1 = coe(4);




