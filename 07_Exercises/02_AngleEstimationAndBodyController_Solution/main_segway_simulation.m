%% Initialize parameters
mdl_segway_param;
par.t_sim = 3;
par.simopt.Solver = 'ode4';
par.simopt.SrcWorkspace = 'current';
par.modelname   = 'mdl_segway_simulation';

% Initial conditions
phi0            = -1;
phid0           = 0;
theta0          = 5.6 / 360 * 2 * pi;
thetad0         = 0;

%% Open-loop system
K_lqr           = [0 0 0 0];

% Run simulation
simout          = sim(par.modelname,par.simopt);
data            = aux_segway_plot(simout,par);

%% Closed-loop system
load linearized_segway_ss
% Design LQR controller
Q               = diag([1 1e-2 1 1e-2]);        % weighting matrix Q - penalize states
R               = 100;                          % weighting matrix R - penalize actuation
K_lqr           = lqr(G_S,Q,R);                 % LQR gain matrix
Kd_lqr          = lqrd(G_S.A,G_S.B,Q,R,par.Ts); % time-discrete LQR gain matrix

% Run simulation
simout          = sim(par.modelname,par.simopt);
data            = aux_segway_plot(simout,par);